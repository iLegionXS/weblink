@extends('layouts.app')

@section('title', 'Пользовательские данные')

@extends('layouts.nav')

@section('content')

    <div class="profile-settings">
        <div class="container">
            <div class="row">
                <div class="col-3">
                    <div class="avatar shadow pt-3 pb-3 rounded">
                        @if(Auth::user()->avatar)
                            <img src="{{ $pathToAvatar }}"
                                 class="w-100 mb-3"
                                 alt="">
                        @else
                            <i class="far fa-user mb-3"></i>
                        @endif
                        <form action="/profile/settings/save/avatar" method="post" enctype="multipart/form-data">
                            @csrf
                            <label for="avatar"
                                   class="btn btn-outline-primary change-photo mb-0">{{ __('profile.photo') }}</label>
                            <input type="file" name="avatar" class="form-control-file d-none" id="avatar"
                                   onchange="this.form.submit();">
                        </form>
                    </div>
                </div>
                <div class="col-6">
                    <div class="profile-user-data">
                        <form action="/profile/settings/save/userData" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="user-name">{{ __('profile.user name') }}</label>
                                <input type="text" class="form-control" id="user-name" name="name"
                                       value="{{ Auth::user()->name }}">
                            </div>
                            <div class="form-group">
                                <label for="user-status">{{ __('profile.user status') }}</label>
                                <input type="text" class="form-control" name="status" id="user-status"
                                       value="{{ Auth::user()->status }}">
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="user-sex">{{ __('profile.sex') }}</label>
                                    <select id="user-sex" class="form-control" name="sex">
                                        @if(Auth::user()->sex)
                                            <option value="{{ Auth::user()->sex }}"
                                                    selected>{{ __('profile.' . Auth::user()->sex) }}</option>
                                        @else
                                            <option selected>{{ __('profile.select') }}...</option>
                                        @endif
                                        <option value="male">{{ __('profile.male') }}</option>
                                        <option value="female">{{ __('profile.female') }}</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="user-phone">{{ __('profile.phone') }}</label>
                                    <input type="text" class="form-control" name="phone" id="user-phone" value="{{ Auth::user()->phone }}">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">{{ __('profile.save') }}</button>
                        </form>
                    </div>
                </div>
                <div class="col-3">
                    <div class="profile-menu">
                        <div class="list-group">
                            <a href="/profile"
                               class="list-group-item list-group-item-action @if(Route::currentRouteName() === 'profileSettings') active @endif">
                                {{ __('profile.profile') }}
                            </a>
                            <a href="/profile/settings/userData"
                               class="list-group-item list-group-item-action @if(Route::currentRouteName() === 'userData') active @endif">
                                {{ __('profile.user data') }}
                            </a>
                            <a href="/profile/password"
                               class="list-group-item list-group-item-action">{{ __('profile.change password') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
