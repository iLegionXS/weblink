@extends('layouts.app')

@section('title', 'Профиль')

@extends('layouts.nav')

@section('content')

    <div class="profile-usage-group">
        <div class="container">
            <div class="row">
                <div class="col-9">
                    <div class="btn-group p-5" role="group" aria-label="{{ __('index.filter') }}">
                        <button type="button" class="btn btn-outline-primary active">Основное</button>
                        <button type="button" class="btn btn-outline-primary">Комментарии</button>
                        <button type="button" class="btn btn-outline-primary">Понравившиеся</button>
                    </div>
                </div>
                <div class="col-3">
                    <div class="profile-menu">
                        <div class="list-group">
                            <a href="/profile"
                               class="list-group-item list-group-item-action @if(Route::currentRouteName() === 'profileSettings') active @endif">
                                {{ __('profile.profile') }}
                            </a>
                            <a href="/profile/settings/userData"
                               class="list-group-item list-group-item-action @if(Route::currentRouteName() === 'userData') active @endif">
                                {{ __('profile.user data') }}
                            </a>
                            <a href="/profile/password"
                               class="list-group-item list-group-item-action">{{ __('profile.change password') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="profile">
        <div class="container">
            <div class="row">
                <div class="col-9">
                    <div class="view-block">
                        <div class="profile-view-main">
                            <p>Дата регистрации: {{ $created_at }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--    <div class="container">--}}
    {{--        <div class="row justify-content-center">--}}
    {{--            <div class="col-md-8">--}}
    {{--                <div class="card">--}}
    {{--                    <div class="card-header">Dashboard</div>--}}

    {{--                    <div class="card-body">--}}
    {{--                        @if (session('status'))--}}
    {{--                            <div class="alert alert-success" role="alert">--}}
    {{--                                {{ session('status') }}--}}
    {{--                            </div>--}}
    {{--                        @endif--}}

    {{--                        You are logged in!--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}

@endsection
