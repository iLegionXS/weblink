@extends('layouts.app')

@section('title', 'Главная')

@extends('layouts.nav')

@section('content')

    <div class="publications">
        <div class="container">
            <div class="row">
                <div class="col">
                    @include('layouts.publication-card')
                </div>
            </div>
        </div>
    </div>

    <div class="publications-filter fixed-bottom">
        <div class="container">
            <div class="row">
                <div class="col d-flex justify-content-center">
                    <div class="btn-group p-5" role="group" aria-label="{{ __('index.filter') }}">
                        @foreach($categories as $item)
                            <button type="button" class="btn btn-outline-primary">{{ $item->name }}</button>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection