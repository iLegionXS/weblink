@foreach($publications as $item)

    <div class="card mb-3">
        <img src="/images/publications/{{ $item->images }}" class="card-img-top" alt="Изображение">
        <div class="card-body">
            <h5 class="card-title">{{ $item->title }}</h5>
            <p class="card-text">{{ $item->description }}</p>
            <p class="card-text">
                <small class="text-muted">Last updated {{ $item->updated_at }} ago</small>
            </p>
        </div>
    </div>

@endforeach