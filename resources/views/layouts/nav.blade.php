@section('nav')

    <div id="nav" class="bg-primary mb-3">
        <div class="container">
            <div class="row">
                <div class="col">
                    <nav class="navbar navbar-expand-lg navbar-dark">
                        <a class="navbar-brand" href="/">WebLink</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown">
                            <ul class="navbar-nav">
                                <li class="nav-item @if(Route::currentRouteName() === 'publications') active @endif">
                                    <a class="nav-link" href="/">{{ __('nav.publications') }} <span class="sr-only">({{ __('nav.current') }})</span></a>
                                </li>
                            </ul>
                        </div>
                        @guest
                            <ul class="navbar-nav nav-auth">
                                <li>
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('nav.login') }}</a>
                                </li>
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('nav.register') }}</a>
                                    </li>
                                @endif
                            </ul>
                        @endguest
                        @auth
                            <div class="navbar-nav navbar-profile-menu">
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ Auth::user()->name }}
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="/admin"><i class="fas fa-wrench pr-3"></i> {{ __("nav.admin panel") }}</a>
                                        <a class="dropdown-item" href="/profile"><i class="fas fa-user pr-3"></i> {{ __("nav.profile") }}</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();" v-pre>
                                            {{ __('nav.logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            </div>
                        @endauth
                    </nav>
                </div>
            </div>
        </div>
    </div>

@endsection