@extends('layouts.app')

@section('title', 'Панель администратора')

@extends('layouts.nav')

@section('content')

    <div class="admin-links">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="card-columns">
                        <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
                            <div class="card-header"><a href="/admin/publication" class="text-light">Публикации</a></div>
                            <div class="card-body">
                                <p class="card-text">
                                    CRUD публикаций.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection