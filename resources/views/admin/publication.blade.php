@extends('layouts.app')

@section('title', 'Добавить публикацию')

@extends('layouts.nav')

@section('content')

    <div class="publication">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="publication-search">
                        <form class="form-inline">
                            <input class="form-control mr-2" type="search" placeholder="ID публикации"
                                   aria-label="Поиск">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Найти</button>
                        </form>
                    </div>
                    <div class="publication-form">
                        <form action="/admin/publication" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="category">Категория</label>
                                <select class="form-control" id="category" name="category">
                                    @foreach($categories as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="title">Название</label>
                                <input type="text" class="form-control" id="title" name="title">
                            </div>
                            <div class="form-group">
                                <label for="description">Описание</label>
                                <textarea class="form-control" id="description" rows="3" name="description"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="images">Изображения</label>
                                <input type="file" class="form-control-file" id="images" name="images[]">
                            </div>
                            <button type="submit" class="btn btn-primary">Добавить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
