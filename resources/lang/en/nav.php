<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Translate navigation
    |--------------------------------------------------------------------------
    */

    'login' => 'Login',
    'register' => 'Register',
    'publications' => 'Publications',
    'current' => 'current',
    'admin panel' => 'Admin Panel',
    'logout' => 'Logout',
    'profile' => 'Profile',
];
