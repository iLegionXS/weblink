<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Translate profile
    |--------------------------------------------------------------------------
    */

    'photo' => 'Change photo',
    'user data' => 'User Data',
    'user name' => 'Real name',
    'user status' => 'Status',
    'sex' => 'Sex',
    'select' => 'Select',
    'male' => 'Male',
    'female' => 'Female',
    'save' => 'Save',
    'change password' => 'Password reset',
    'birthday' => 'Birthday',
    'profile' => 'Profile',
    'Номер телефона' => 'Phone',
];