<?php

use Illuminate\Database\Seeder;
use WebLink\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'iLegionXS';
        $user->email = 'wordlearningpress@gmail.com';
        $user->password = bcrypt('yusif1999');
        $user->real_name = 'Yusif Zourab';
        $user->status = 'Admin';
        $user->sex = 'male';
        $user->avatar = '1.png';
        $user->save();
    }
}
