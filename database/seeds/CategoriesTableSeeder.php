<?php

use Illuminate\Database\Seeder;
use WebLink\Model\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = new Category();
        $category->name = 'HTML';
        $category->save();

        $category = new Category();
        $category->name = 'CSS';
        $category->save();

        $category = new Category();
        $category->name = 'JS';
        $category->save();

        $category = new Category();
        $category->name = 'PHP';
        $category->save();

        $category = new Category();
        $category->name = 'WebLink';
        $category->save();
    }
}
