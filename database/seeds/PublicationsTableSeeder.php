<?php

use Illuminate\Database\Seeder;
use WebLink\Model\Publication;

class PublicationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $publication = new Publication();
        $publication->category_id = 5;
        $publication->user_id = 1;
        $publication->title = 'WebLink: Начало';
        $publication->images = json_encode('1.jpg');
        $publication->description = 'Здравствуйте, уважаемый гость сайта. Мы наконец-то открылись.';
        $publication->is_moderate = 1;
        $publication->is_published= 1;
        $publication->published_at = date("Y-m-d H:i:s");
        $publication->save();
    }
}
