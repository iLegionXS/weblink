<?php

namespace WebLink\Http\Controllers\Admin;

use Illuminate\Http\Request;
use WebLink\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        return view('admin/index');
    }
}
