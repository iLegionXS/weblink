<?php

namespace WebLink\Http\Controllers\Admin;

use Illuminate\Http\Request;
use WebLink\Http\Controllers\Controller;
use WebLink\Model\Category;

class PublicationController extends Controller
{
    public function index()
    {
        return view('admin.publication', [
            'categories' => Category::all(),
        ]);
    }

    public function ()
    {

    }
}
