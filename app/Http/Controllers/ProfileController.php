<?php

namespace WebLink\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use WebLink\User;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $time = strtotime(Auth::user()->created_at);
        $time = date('d-m-Y h:i:s', $time);

        return view('profile.profile', [
            'created_at' => $time,
        ]);
    }

    public function showUserDataPage()
    {
        return view('profile.profile-settings-user-data', [
            'pathToAvatar' => Storage::url(Auth::user()->avatar),
        ]);
    }

    public function saveUserData(Request $request)
    {
        $request->validate([
            'name' => 'unique:users|string|nullable|between:3,30',
//            'status' => 'string|between:5,150|nullable',
//            'status' => 'alpha_dash',
//            'sex' => 'string|nullable',
//            'phone' => 'integer|size:10|nullable',
        ]);

        $user = new User();
        $item = $user::find(Auth::user()->id);
        $userData = [
            'name' => $request->name,
            'status' => $request->status,
            'sex' => $request->sex,
            'phone' => $request->phone,
        ];
        $item->fill($userData);
        $item->save();

        return redirect('/profile/settings/userData');
    }

    public function saveAvatar(Request $request)
    {
        $request->validate([
            'avatar' => 'required|image',
        ]);

        $path = $request->file('avatar')->store('public/users/' . Auth::user()->name);

        $user = new User();
        $item = $user::find(Auth::user()->id);
        $item->avatar = $path;
        $item->save();

        return redirect('/profile/settings/userData');
    }
}
