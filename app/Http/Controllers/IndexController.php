<?php

namespace WebLink\Http\Controllers;

use Illuminate\Http\Request;
use WebLink\Model\Category;
use WebLink\Model\Publication;

class IndexController extends Controller
{
    public function index()
    {
        $publications = Publication::all();

        foreach ($publications as $item) {
            $item->images = json_decode($item->images);
        }

        return view('index', [
            'categories' => Category::all(),
            'publications' => $publications,
        ]);
    }
}
