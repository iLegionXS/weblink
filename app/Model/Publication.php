<?php

namespace WebLink\Model;

use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
    protected $fillable = [
        'category_id',
        'user_id',
        'title',
        'images',
        'description',
    ];
}
