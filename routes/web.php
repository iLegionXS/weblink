<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('publications');

Auth::routes();

Route::get('/profile', 'ProfileController@index')->name('profileSettings');
Route::get('/profile/settings/userData', 'ProfileController@showUserDataPage')->name('userData');

Route::post('/profile/settings/save/userData', 'ProfileController@saveUserData');
Route::post('/profile/settings/save/avatar', 'ProfileController@saveAvatar');

Route::namespace('Admin')->group(function () {
    Route::get('/admin/settings/userData', 'IndexController@index');
    Route::get('/admin/publication', 'PublicationController@index');

    Route::post('/admin/publication', 'PublicationController@');
});
